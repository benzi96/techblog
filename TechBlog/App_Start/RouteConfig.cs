﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TechBlog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "SlugRoute",
                url: "{controller}/{id}/{urlslug}",
                defaults: new { action = "Details" }
            );
            routes.MapRoute(
                name: "ManagePosts",
                url: "ManagePosts/{action}/{id}/{urlslug}",
                defaults: new { controller = "ManagePosts", action = "Index" }
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
