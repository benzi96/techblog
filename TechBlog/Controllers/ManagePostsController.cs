﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlogEntity;
using TechBlog.Models;
using System.Drawing;
using System.IO;
using BlogEntity.UtilityHelper;
using AutoMapper;
using Microsoft.AspNet.Identity;
using BlogDTO;
using PagedList;
using TechBlog.API;
using System.Net.Http;
using System.Web.Security;

namespace TechBlog.Controllers
{
    [Authorize]
    public class ManagePostsController : Controller
    {
        Client client;
        public ManagePostsController()
        {
            client = new Client();
            ViewBag.categories = client.Get<List<CategoryDTO>>("categories");
        }

        public string CheckFileValid(PostDTO postDTO, HttpPostedFileBase file, bool IsNew)
        {
            if (file != null && file.ContentLength > 0 && file.ContentLength < 2 * 1024 * 1024)
            {
                var filename = Path.GetFileName(file.FileName);
                String ext = Path.GetExtension(file.FileName);
                if (ImageHelper.IsImage(ext))
                {
                    Image sourceimage = Image.FromStream(file.InputStream);
                    postDTO.PostImage = ImageHelper.imageToByteArray(ImageHelper.ResizeImage(sourceimage, 600, 600));
                }
                else
                {
                    return "File upload is not Image type.";
                }
            }
            else if (IsNew && file == null)
            {
                return "There is no file upload.";
            }
            else if (file != null && file.ContentLength < 2 * 1024 * 1024)
            {
                return "File upload is larger than 2 MB.";
            }
            return "";
        }

        public ActionResult ReturnViewWithCategories(PostFormViewModel post = null)
        {
            int categoryId = post == null ? 1 : post.CategoryId;
            ViewBag.Category = new SelectList(client.Get<List<CategoryDTO>>("categories"), "Id", "Name", categoryId);
            return View(post);
        }

        public ActionResult Index(int? page)
        {
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            int totalUserPost = client.Get<int>(string.Format("posts?Username={0}", User.Identity.GetUserName()));
            var posts = client.Get<List<PostDTO>>(string.Format("posts?Username={0}&page={1}", User.Identity.GetUserName(), page));
            var postmetaviewmodels = Mapper.Map<List<PostDTO>, List<PostMetaViewModel>>(posts);
            var postsAsIPagedList = new StaticPagedList<PostMetaViewModel>(postmetaviewmodels, pageIndex + 1, pageSize, totalUserPost);

            ViewBag.OnePageOfUserPosts = postsAsIPagedList;
            return View();
        }

        public ActionResult Create()
        {
            return ReturnViewWithCategories();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title, Body, CategoryId, Tags")] PostFormViewModel post, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {   
                PostDTO postDTO = new PostDTO();
                postDTO = Mapper.Map<PostDTO>(post);
                string exception = CheckFileValid(postDTO, file, true);
                if (exception == "")
                {
                    //string cookieName = FormsAuthentication.FormsCookieName;
                    //HttpCookie myCookie = new HttpCookie(cookieName);
                    //myCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

                    postDTO.Username = User.Identity.GetUserName();
                    var statuscode = client.Post<PostDTO>(postDTO, "ManagePosts", Request.Cookies[FormsAuthentication.FormsCookieName]);
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", exception);
                return ReturnViewWithCategories(post);
            }
            return ReturnViewWithCategories(post);
        }

        public ActionResult Edit(int? id)
        {
            var post = client.Get(string.Format("ManagePosts/{0}?username={1}", id, User.Identity.GetUserName()), Request.Cookies[FormsAuthentication.FormsCookieName]);
            if(!post.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            var postviewmodel = Mapper.Map<PostFormViewModel>(post.Content.ReadAsAsync<PostDTO>().Result);
            return ReturnViewWithCategories(postviewmodel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Title, Body, CategoryId, Tags, RowVersion")] PostFormViewModel post, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                PostDTO postDTO = Mapper.Map<PostDTO>(post);
                string exception = CheckFileValid(postDTO, file, false);
                if (exception == "")
                {
                    postDTO.Id = post.Id;
                    postDTO.Username = User.Identity.GetUserName();
                    var response = client.Put(postDTO, "ManagePosts", Request.Cookies[FormsAuthentication.FormsCookieName]);
                    if(response.IsSuccessStatusCode) return RedirectToAction("Index");
                    else
                    {
                        ModelState.AddModelError("", response.Content.ReadAsStringAsync().Result);
                        return ReturnViewWithCategories(post);
                    }
                }
                return ReturnViewWithCategories(post);
            }
            return ReturnViewWithCategories(post);
        }
        
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var response = client.Get("ManagePosts/" + id + "?Username=" + User.Identity.GetUserName(), Request.Cookies[FormsAuthentication.FormsCookieName]);
            if (!response.IsSuccessStatusCode) return RedirectToAction("Index");
            var postviewmodel = Mapper.Map<PostViewModel>(response.Content.ReadAsAsync<PostDTO>().Result);
            postviewmodel.PopularTags = client.Get<List<TagDTO>>("Tags");
            postviewmodel.RecentPosts = Mapper.Map<List<PostMetaViewModel>>(client.Get<List<PostDTO>>("Posts"));
            return View(postviewmodel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client.Delete("ManagePosts/" + id + "?username=" + User.Identity.GetUserName(), Request.Cookies[FormsAuthentication.FormsCookieName]);
            return RedirectToAction("Index");
        }
    }
}
