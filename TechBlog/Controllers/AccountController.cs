﻿using AutoMapper;
using BlogDTO;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TechBlog.API;
using TechBlog.Models;

namespace TechBlog.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        Client client;
        public AccountController()
        {
            client = new Client();
            ViewBag.categories = client.Get<List<CategoryDTO>>("categories");
        }
        // GET: Account
        public ActionResult Index()
        {
            return RedirectToAction("Index", "ManagePosts");
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            if(Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "ManagePosts");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            UserDTO userDTO = Mapper.Map<UserDTO>(model);
            var response = client.Post<UserDTO>(userDTO, "account/login");
            if (response.IsSuccessStatusCode)
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Signup()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "ManagePosts");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Signup(SignupViewModel model)
        {
            if(ModelState.IsValid)
            {
                UserDTO user = Mapper.Map<UserDTO>(model);
                var response = client.Post<UserDTO>(user, "account/signup");
                if (response.IsSuccessStatusCode)
                {
                    LoginViewModel loginViewModel = new LoginViewModel();
                    loginViewModel.Username = model.Username;
                    loginViewModel.Password = model.Password;
                    return Login(loginViewModel);
                }
                else
                {
                    ModelState.AddModelError("Username", "Username already exists");
                    return View();
                }
            }
            return View();
        }
    }
}