﻿using AutoMapper;
using BlogDTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TechBlog.API;
using TechBlog.Models;

namespace TechBlog.Controllers
{
    public class TagsController : Controller
    {
        Client client;
        public TagsController()
        {
            client = new Client();
            ViewBag.categories = client.Get<List<CategoryDTO>>("categories");
        }
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            int totalTagPosts = client.Get<int>("tags/" + id);
            var tagDTO = client.Get<TagDTO>("tags/" + id + "?page=" + page);
            var tagviewmodel = Mapper.Map<TagViewModel>(tagDTO);
            var TagPostsAsIPagedList = new StaticPagedList<PostMetaViewModel>(tagviewmodel.Posts, pageIndex + 1, pageSize, totalTagPosts);
            ViewBag.Id = tagDTO.Id;
            ViewBag.TagName = tagDTO.Name;
            ViewBag.UrlSlug = tagDTO.UrlSlug;
            ViewBag.OnePageOfTagPosts = TagPostsAsIPagedList;
            //ViewBag.PageCount = TagPostsAsIPagedList.PageCount;
            return View();
        }
    }
}