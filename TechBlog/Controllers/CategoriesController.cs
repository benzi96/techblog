﻿using AutoMapper;
using BlogDTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TechBlog.API;
using TechBlog.Models;

namespace TechBlog.Controllers
{
    public class CategoriesController : Controller
    {
        Client client;
        public CategoriesController()
        {
            client = new Client();
            ViewBag.categories = client.Get<List<CategoryDTO>>("categories");
        }
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            int totalCategoryPost = client.Get<int>(string.Format("categories/{0}", id));
            var category = client.Get<CategoryDTO>(string.Format("categories/{0}?page={1}", id, page));
            var categoryviewmodel = Mapper.Map<CategoryViewModel>(category);
            var CategoryPostsAsIPagedList = new StaticPagedList<PostMetaViewModel>(categoryviewmodel.Posts, pageIndex + 1, pageSize, totalCategoryPost);
            ViewBag.Id = category.Id;
            ViewBag.CategoryName = category.Name;
            ViewBag.UrlSlug = category.UrlSlug;
            ViewBag.OnePageOfCategoryPosts = CategoryPostsAsIPagedList;
            return View();
        }
    }
}