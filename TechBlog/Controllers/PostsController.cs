﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TechBlog.Models;
using AutoMapper;
using Microsoft.AspNet.Identity;
using BlogDTO;
using BlogEntity.UtilityHelper;
using PagedList;
using TechBlog.API;

namespace TechBlog.Controllers
{
    public class PostsController : Controller
    {
        Client client;
        public PostsController()
        {
            client = new Client();
            ViewBag.categories = client.Get<List<CategoryDTO>>("categories");
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var post = client.Get<PostDTO>("posts/" + id);
            var postviewmodel = Mapper.Map<PostDTO, PostViewModel>(post);
            postviewmodel.PopularTags = client.Get<List<TagDTO>>("tags");
            postviewmodel.RecentPosts = Mapper.Map<List<PostMetaViewModel>>(client.Get<List<PostDTO>>("posts"));
            return View(postviewmodel);
        }
        public ActionResult GetUserPosts(string UserName, int? page)
        {
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            int totalUserPost = client.Get<int>("posts?Username=" + UserName);
            var posts = client.Get<List<PostDTO>>("posts?Username=" + UserName + "&page=" + page);
            var postmetaviewmodels = Mapper.Map<List<PostDTO>, List<PostMetaViewModel>>(posts);
            var postsAsIPagedList = new StaticPagedList<PostMetaViewModel>(postmetaviewmodels, pageIndex + 1, pageSize, totalUserPost);

            ViewBag.UserName = UserName;
            ViewBag.OnePageOfUserPosts = postsAsIPagedList;
            return View();
        }
       
        public ActionResult RecentPosts()
        {
            var recentPosts = client.Get<List<PostDTO>>("posts");
            List <PostMetaViewModel> postMetaViewModel = new List<PostMetaViewModel>();
            Mapper.Map(recentPosts, postMetaViewModel);
            return View(postMetaViewModel);
        }
        public ActionResult SearchPage()
        {
            List<string> options = new List<string>(new string[] { "Title", "Author", "Tag" });
            ViewBag.option = new SelectList(options);
            return View();
        }
        public ActionResult SearchResult(string term, string option)
        {
            List<PostDTO> posts = new List<PostDTO>();
            List<TagDTO> tags = new List<TagDTO>();
            if (option == "Title" || option == "Author")
                posts = client.Get<List<PostDTO>>("posts?term=" + term + "&option=" + option);
            else
                tags = client.Get<List<TagDTO>>("posts?term=" + Uri.EscapeDataString(term) + "&option=" + option);
            SearchResultViewModel searchResultViewModel = new SearchResultViewModel();
            searchResultViewModel.posts = Mapper.Map<List<PostMetaViewModel>>(posts);
            searchResultViewModel.tags = Mapper.Map<List<TagViewModel>>(tags);
            return PartialView("_SearchResultPartial", searchResultViewModel);
        }
    }
}
