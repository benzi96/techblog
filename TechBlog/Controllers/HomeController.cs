﻿using AutoMapper;
using BlogDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TechBlog.API;
using TechBlog.Controllers;
using TechBlog.Models;

namespace blog.Controllers
{
    public class HomeController : Controller
    {
        Client client;
        public HomeController()
        {
            client = new Client();
            ViewBag.categories = client.Get<List<CategoryDTO>>("categories");
        }

        public ActionResult Index()
        {
            var categories = client.Get<List<CategoryDTO>>("Home");
            var categoryviewmodels = Mapper.Map<List<CategoryViewModel>>(categories);
            return View(categoryviewmodels);
        }
    }
}