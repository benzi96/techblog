﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlogEntity.entity;
using BlogEntity.service;
using TechBlog.Models;
using BlogEntity.DTO;
using AutoMapper;
using Microsoft.AspNet.Identity;

namespace TechBlog.Controllers
{
    public class ViewPostsController : Controller
    {
        IPostService _postService;
        public ViewPostsController(IPostService postService)
        {
            _postService = postService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var post=_postService.GetPost(id);
            var postviewmodel = Mapper.Map<PostDTO, PostViewModel>(post);
            return View(postviewmodel);
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
