﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TechBlog.Models;
using AutoMapper;
using Microsoft.AspNet.Identity;
using BlogDTO;
using TechBlog.API;
using System.Net.Http;
using System.Web.Security;

namespace TechBlog.Controllers
{
    [Authorize]
    public class CommentsController : Controller
    {
        Client client;
        public CommentsController()
        {
            client = new Client();
        }
        [HttpPost]
        public ActionResult Create(CommentViewModel commentviewmodel)
        {
            CommentDTO commentDTOFromAPI = new CommentDTO();
            CommentDTO commentDTO = Mapper.Map<CommentDTO>(commentviewmodel);
            commentDTO.Username = User.Identity.GetUserName();
            commentDTO.CommentedOn = DateTime.Now;

            commentDTOFromAPI = client.Post(commentDTO, "Comments", Request.Cookies[FormsAuthentication.FormsCookieName]).Content.ReadAsAsync<CommentDTO>().Result;
            return PartialView("_CommentPartial", commentDTOFromAPI);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void Delete(int? Id)
        {
            if (Id != null) client.Delete("Comments/" + Id + "?username=" + User.Identity.GetUserName(), Request.Cookies[FormsAuthentication.FormsCookieName]);
        }
    }
}
