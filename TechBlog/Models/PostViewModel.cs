﻿using BlogDTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TechBlog.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UrlSlug { get; set; }
        public string PostImageSrc { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public string ShortDescription { get; set; }
        public DateTime PostedOn { get; set; }
        public int CategoryID { get; set; }
        public string Username { get; set; }
        public List<TagDTO> Tags { get; set; }
        public List<CommentDTO> Comments { get; set; }

        public List<TagDTO> PopularTags { get; set; }
        public List<PostMetaViewModel> RecentPosts { get; set; }
    }
}