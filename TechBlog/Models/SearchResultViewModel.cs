﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechBlog.Models
{
    public class SearchResultViewModel
    {
        public List<PostMetaViewModel> posts;
        public List<TagViewModel> tags;
    }
}