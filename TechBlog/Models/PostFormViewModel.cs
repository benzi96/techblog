﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TechBlog.Models
{
    public class PostFormViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        [AllowHtml]
        public string Body { get; set; }
        public int CategoryId { get; set; }

        public byte[] RowVersion { get; set; }

        public List<TagViewModel> Tags { get; set; }
        public string Tagname { get; set; }
    }
}