﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechBlog.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string  UrlSlug { get; set; }
        public List<PostMetaViewModel> Posts { get; set; }
    }
}