﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechBlog.Models
{
    public class PostMetaViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UrlSlug { get; set; }
        public string PostImageSrc { get; set; }
        public string ShortDescription { get; set; }
        public DateTime PostedOn { get; set; }
        public string Username { get; set; }
    }
}