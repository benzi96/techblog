﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechBlog.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public DateTime CommentedOn { get; set; }

        public string Username { get; set; }
        public int PostId { get; set; }
    }
}