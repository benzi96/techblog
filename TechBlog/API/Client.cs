﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace TechBlog.API
{
    public class Client
    {
        private Uri baseAddress;

        public Client(Uri baseAddress)
        {
            this.baseAddress = baseAddress;
        }
        public Client()
        {
            this.baseAddress = new Uri(ConfigurationManager.AppSettings.Get("BaseApiUrl"));
        }

        public T Get<T>(string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                var response = httpClient.GetAsync(query).Result;
                return response.Content.ReadAsAsync<T>().Result;
            }
        }

        public HttpResponseMessage Get(string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                var response = httpClient.GetAsync(query).Result;
                return response;
            }
        }

        public HttpResponseMessage Get(string query, HttpCookie cookie)
        {
            using (var httpClient = new HttpClient())
            {
                var cookieContainer = new CookieContainer();
                using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
                using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
                {
                    cookieContainer.Add(baseAddress, new Cookie(cookie.Name, cookie.Value));
                    var result = client.GetAsync(query).Result;
                    return result;
                }
            }
        }

        //public bool Post<T>(T entity, string query)
        //{
        //    using (var httpClient = new HttpClient())
        //    {
        //        httpClient.BaseAddress = baseAddress;
        //        var response = httpClient.PostAsJsonAsync(query, entity).Result;
        //        return response.IsSuccessStatusCode;
        //    }
        //}
        public HttpResponseMessage Post<T>(T entity, string query, HttpCookie cookie)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.BaseAddress = baseAddress;
                //var response = httpClient.PostAsJsonAsync(query, entity).Result;
                //return response;
                var cookieContainer = new CookieContainer();
                using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
                using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
                {
                    cookieContainer.Add(baseAddress, new Cookie(cookie.Name, cookie.Value));
                    var result = client.PostAsJsonAsync(query, entity).Result;
                    return result;
                }
            }
        }
        public HttpResponseMessage Post<T>(T entity, string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                var response = httpClient.PostAsJsonAsync(query, entity).Result;
                return response;
            }
        }

        public HttpResponseMessage Put<T>(T entity, string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                var response = httpClient.PutAsJsonAsync(query, entity).Result;
                return response;
            }
        }

        public HttpResponseMessage Put<T>(T entity, string query, HttpCookie cookie)
        {
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                cookieContainer.Add(baseAddress, new Cookie(cookie.Name, cookie.Value));
                var result = client.PutAsJsonAsync(query, entity).Result;
                return result;
            }
        }

        public bool Delete(string query, HttpCookie cookie)
        {
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                cookieContainer.Add(baseAddress, new Cookie(cookie.Name, cookie.Value));
                var result = client.DeleteAsync(query).Result;
                return result.IsSuccessStatusCode;
            }
        }
    }
}