﻿using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using BlogDTO;
using BlogEntity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TechBlog.AutoMapperProfile;
using TechBlog.Models;

namespace TechBlog
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<PostModelProfile>();
                cfg.CreateMap<SignupViewModel, UserDTO>();
                cfg.CreateMap<CategoryDTO, CategoryViewModel>();
                cfg.CreateMap<CommentDTO, CommentViewModel>().ReverseMap();
                cfg.CreateMap<TagDTO, TagViewModel>().ReverseMap();
            });
        }
    }
}
