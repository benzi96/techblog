﻿using AutoMapper;
using BlogDTO;
using BlogEntity.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechBlog.Models;

namespace TechBlog.AutoMapperProfile
{
    public class PostModelProfile : Profile
    {
        public PostModelProfile()
        {
            CreateMap<PostDTO, PostViewModel>()
                .ForMember(dest => dest.PostImageSrc, opt => opt.MapFrom(src => ImageHelper.GetImageTagSrc(src.PostImage)));
            CreateMap<PostDTO, PostMetaViewModel>()
                .ForMember(dest => dest.PostImageSrc, opt => opt.MapFrom(src => ImageHelper.GetImageTagSrc(src.PostImage)));

            CreateMap<PostViewModel, PostDTO>();

            CreateMap<PostFormViewModel, PostDTO>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.UrlSlug, opt => opt.Ignore())
                .ForMember(dest => dest.PostImage, opt => opt.Ignore())
                .ForMember(dest => dest.ShortDescription, opt => opt.Ignore())
                .ForMember(dest => dest.PostedOn, opt => opt.Ignore())
                .ForMember(dest => dest.Modified, opt => opt.Ignore())
                .ForMember(dest => dest.Username, opt => opt.Ignore())
                .ForMember(dest => dest.Comments, opt => opt.Ignore());
            CreateMap<PostDTO, PostFormViewModel>()
               .ForMember(dest => dest.Tagname, opt => opt.Ignore());
        }
    }
}