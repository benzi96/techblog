﻿ $(function() {
     $('#myEditor').froalaEditor({
         toolbarInline: false
     })
});

 var dynamicId = 0

 $("#tag").on('keydown', function (e) {
     if (e.which === 13 || e.keyCode === 13) {
         dynamicId += 1;
         $('#listtags').append(
             "<li>"+
             '<button type="button" class="btn">' + $('#tag').val() + '</button> ' +
             '<input type="hidden" name="Tags.Index" value="' + (dynamicId - 1) + '" />' +
             '<input type="hidden" name="Tags[' + (dynamicId - 1) + '].Name" value="' + $('#tag').val() + '" />' +
             "</li>"
         )
         $("#tag").val('');
         return false;
     }
 });

 $("#listtags").on('click', '.btn', function () {
     $(this).closest('li').find("input[type='hidden']").remove();
     $(this).closest('li').remove();
 });

$('#searchinput').keydown(function (e) {
  if (e.which === 13) {
    $('form#formsearch').submit();
    return false;
  }
});

$('.search-select2').select2({
    ajax: {
        url: "http://localhost:60311/api/Posts",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                term: params.term, // search term
            };
        },
        processResults: function (data, params) {
            return {
                results: data,
            };
        },
        cache: true
    },
    placeholder: 'Search for a post title',
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
});

function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    var markup = "<div class='row clearfix searchresult'>" +
        "<div class='col-md-4'><img id='image' class='img-rounded' src='" + repo.imgsrc + "' /></div>" +
        "<div class='col-md-7'><h4>" + repo.title + " - " + repo.author + "</h4></div>";

    markup += "</div>";

    return markup;
}

function formatRepoSelection(repo) {
    if (repo.url) {
        window.location.replace(repo.url);
        return 'redirecting to post...';
    }
    return repo.title || repo.text;
}