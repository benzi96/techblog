﻿using AutoMapper;
using BlogEntity.Entity;
using BlogService.Service;
using DAL;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BlogUnitTest.CommentServiceTest
{
    public class CommentServiceTest
    {
        Mock<IGenericRepository<Comment>> mockCommentRepository;
        Mock<IUnitOfWork> mockUnitOfWork;
        CommentService commentService;
        public CommentServiceTest()
        {
            //Mapper.Reset();
            mockCommentRepository = new Mock<IGenericRepository<Comment>>();
            mockUnitOfWork = new Mock<IUnitOfWork>();
            commentService = new CommentService(mockUnitOfWork.Object, mockCommentRepository.Object);
        }
        [Fact]
        public void DeleteComment_if_this_comment_not_belongs_to_this_user_it_should_not_be_deleted()
        {
            mockCommentRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Comment, bool>>>()))
                                 .Returns((Expression<Func<Comment, bool>> Predicate) =>  new List<Comment>().AsQueryable());
            int error = commentService.DeleteComment(1, "khon");
            Assert.Equal(-1, error);
        }
        [Fact]
        public void DeleteComment_if_this_comment_belongs_to_user_it_should_be_deleted()
        {
            List<Comment> comments = new List<Comment>();
            Comment comment = new Comment();
            comment.Body = "asd";
            comment.Id = 1;
            comment.UserId = 1;
            comment.User = new User();
            comment.User.Username = "khon";
            comments.Add(comment);

            mockCommentRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Comment, bool>>>()))
                                 .Returns((Expression<Func<Comment, bool>> Predicate) => comments.AsQueryable());

            commentService.DeleteComment(1, "khon");
            mockCommentRepository.Verify(x => x.Delete(It.IsAny<Comment>()));
        }
    }
}
