﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogService.Service;
using BlogService.ServiceModule.AutoMapperProfile;
using DAL;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace BlogUnitTest.TagServiceTest
{
    public class TagServiceTest
    {
        Mock<IGenericRepository<Tag>> mockTagRepository;
        Mock<IUnitOfWork> mockUnitOfWork;
        TagService tagService;
        public TagServiceTest()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<TagProfile>();
                cfg.AddProfile<PostProfile>();
            });

            mockTagRepository = new Mock<IGenericRepository<Tag>>();
            mockUnitOfWork = new Mock<IUnitOfWork>();
            tagService = new TagService(mockUnitOfWork.Object, mockTagRepository.Object);
        }
        [Fact]
        public void GetPostsByTag_find_by_id_should_be_call()
        {
            Tag tag = new Tag();
            tag.Posts = new List<Post>();
            mockTagRepository.Setup(x => x.FindById(It.IsAny<int>())).Returns(tag);
            tagService.GetPostsByTag(1, 0, 10);
            mockTagRepository.VerifyAll();
        }

        [Fact]
        public void GetTotalTagPosts_should_return_int_total()
        {
            Post post = new Post();
            Tag tag = new Tag();
            tag.Posts = new List<Post>();
            tag.Posts.Add(post);
            tag.Posts.Add(post);
            tag.Posts.Add(post);

            mockTagRepository.Setup(x => x.FindById(It.IsAny<int>())).Returns(tag);
            int total = tagService.GetTotalTagPosts(1);

            Assert.Equal(3, total);
        }
        [Fact]
        public void GetPopularTags_should_return_8_tag_most_count()
        {
            Post post = new Post();
            Tag tag = new Tag();
            tag.Posts = new List<Post>();
            tag.Posts.Add(post);
            tag.Posts.Add(post);
            tag.Posts.Add(post);

            List<Tag> tags = new List<Tag>();
            tags.Add(tag);
            tags.Add(tag);
            mockTagRepository.Setup(x => x.GetAll()).Returns(tags.AsQueryable());
            var tagDTOs = tagService.GetPopularTags();

            Assert.InRange(tagDTOs.Count(), 0, 8);
        }

        [Fact]
        public void GetTags_by_term_FindAll_should_be_call()
        {
            Post post = new Post();
            Tag tag = new Tag();
            tag.Posts = new List<Post>();
            tag.Posts.Add(post);
            tag.Posts.Add(post);
            tag.Posts.Add(post);

            List<Tag> tags = new List<Tag>();
            tags.Add(tag);
            tags.Add(tag);
            mockTagRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Tag, bool>>>())).Returns(tags.AsQueryable());
            var tagDTOs = tagService.GetTags("microsoft");

            Assert.InRange(tagDTOs.Count(), 0, 8);
        }
    }
}
