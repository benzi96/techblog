﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogService.IService;
using BlogService.Service;
using BlogService.ServiceModule.AutoMapperProfile;
using DAL;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace BlogUnitTest.PostServiceTest
{
    public class PostServiceTest
    {
        Mock<IGenericRepository<Post>> mockPostRepository;
        Mock<IGenericRepository<Tag>> mockTagRepository;
        Mock<IGenericRepository<User>> mockUserRepository;
        Mock<IUnitOfWork> mockUnitOfWork;
        Mock<IUserService> userService;
        PostService postService;
        public PostServiceTest()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<PostProfile>();
                cfg.AddProfile<TagProfile>();
                cfg.AddProfile<CommentProfile>();
            });
            mockPostRepository = new Mock<IGenericRepository<Post>>();
            mockTagRepository = new Mock<IGenericRepository<Tag>>();
            mockUserRepository = new Mock<IGenericRepository<User>>();
            mockUnitOfWork = new Mock<IUnitOfWork>();
            userService = new Mock<IUserService>();
            postService = new PostService(mockUnitOfWork.Object, mockPostRepository.Object, mockTagRepository.Object, userService.Object);
        }
        [Fact]
        public void GetPost_FindAll_should_be_call()
        {
            Post p = new Post();
            p.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(p);
            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            postService.GetPost(1);
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));
        }
        
        [Fact]
        public void GetPost_by_username_this_post_not_belong_to_user_return_null()
        {
            List<Post> posts = new List<Post>();

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var post = postService.GetPost("khon", 1);
            Assert.Null(post);
        }
        [Fact]
        public void GetPost_by_username_this_post_belong_to_user_return_postdto()
        {
            Post post = new Post();
            List<Post> posts = new List<Post>();
            posts.Add(post);
            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var p = postService.GetPost("khon", 1);
            Assert.IsType<PostDTO>(p);
        }

        [Fact]
        public void GetPosts_by_username_FindAll_should_be_call_and_return_list_postdto()
        {
            Post p = new Post();
            p.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(p);
            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var postdtoes = postService.GetPosts("khon", 0, 10);
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));

            Assert.IsType<List<PostDTO>>(postdtoes);
        }
        [Fact]
        public void GetTotalUserPosts_FindAll_should_be_call_and_return_int_total()
        {
            Post p = new Post();
            p.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(p);
            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var postdtoes = postService.GetTotalUserPosts("khon");
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));

            Assert.IsType<int>(postdtoes);
        }

        [Fact]
        public void GetPosts_by_term_and_option_with_option_author_should_return_user_posts()
        {
            Post p1 = new Post();
            p1.Tags = new List<Tag>();
            p1.User = new User();
            p1.User.Username = "khon";
            p1.Title = "hello_world";

            List<Post> posts = new List<Post>();
            posts.Add(p1);

            List<PostDTO> expectedPosts = new List<PostDTO>();
            PostDTO expectedPost = new PostDTO();
            expectedPost.Tags = new List<TagDTO>();
            expectedPost.Username = "khon";
            expectedPost.Title = "hello_world";
            expectedPosts.Add(expectedPost);

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var postdtoes = postService.GetPosts("khon", "Author");
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));

            Assert.Equal(expectedPosts.First().Username, postdtoes.First().Username);
        }
        [Fact]
        public void GetPosts_by_term_and_option_with_option_Title_should_return_posts_with_this_title()
        {
            Post p1 = new Post();
            p1.Tags = new List<Tag>();
            p1.User = new User();
            p1.User.Username = "khon";
            p1.Title = "hello_world";

            List<Post> posts = new List<Post>();
            posts.Add(p1);

            List<PostDTO> expectedPosts = new List<PostDTO>();
            PostDTO expectedPost = new PostDTO();
            expectedPost.Tags = new List<TagDTO>();
            expectedPost.Username = "khon";
            expectedPost.Title = "hello_world";
            expectedPosts.Add(expectedPost);

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var postdtoes = postService.GetPosts("hello_world", "Title");
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));

            Assert.Equal(expectedPosts.First().Title, postdtoes.First().Title);
        }

        [Fact]
        public void GetPostsForAutoComplete_FindAll_should_be_call_return_8_posts_title_contains_string()
        {
            Post p1 = new Post();
            p1.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var postdtoes = postService.GetPostsForAutoComplete("hello_world");
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));

            Assert.Equal(8, postdtoes.Count());
        }

        [Fact]
        public void GetRecentPosts_FindAll_should_be_call_return_6_recent_posts()
        {
            Post p1 = new Post();
            p1.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);
            posts.Add(p1);

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            var postdtoes = postService.GetRecentPosts();
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));

            Assert.Equal(6, postdtoes.Count());
        }
        [Fact]
        public void AddComment_Update_should_be_call()
        {
            CommentDTO commentDTO = new CommentDTO();
            Post post = new Post();
            userService.Setup(x => x.GetUserId(It.IsAny<string>())).Returns(1);
            mockPostRepository.Setup(x => x.FindById(It.IsAny<int>())).Returns(post);

            postService.AddComment(commentDTO);

            mockPostRepository.Verify(x => x.Update(It.IsAny<Post>()));
        }

        [Fact]
        public void AddPost_PostDTO_null_should_throw_exception()
        {
            PostDTO postDTO = null;
            try
            {
                postService.AddPost(postDTO);
            }
            catch (ArgumentNullException ex)
            {
                Assert.Equal("entity", ex.ParamName);
            }
        }

        [Fact]
        public void AddPost_PostDTO_not_null_should_be_created()
        {
            PostDTO postDTO = new PostDTO();
            postDTO.Tags = new List<TagDTO>();

            userService.Setup(x => x.GetUserId(It.IsAny<string>())).Returns((string username) => 1);

            postService.AddPost(postDTO);
            mockPostRepository.Verify(x => x.Add(It.IsAny<Post>()));
        }

        [Fact]
        public void DeletePost_this_post_not_belongs_to_user_should_return_0()
        {
            List<Post> posts = new List<Post>();

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            int error = postService.DeletePost(1, "khon");
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));
            mockPostRepository.VerifyNoOtherCalls();

            Assert.Equal(0, error);
        }

        [Fact]
        public void DeletePost_this_post_belongs_to_user_this_should_be_deleted()
        {
            Post post = new Post();
            post.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(post);
            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            postService.DeletePost(1, "khon");

            mockPostRepository.Verify(x => x.Delete(It.IsAny<Post>()));
        }

        [Fact]
        public void UpdatePost_PostDTO_null_should_throw_exception()
        {
            PostDTO postDTO = null;
            try
            {
                postService.UpdatePost(postDTO, "khon");
            }
            catch (ArgumentNullException ex)
            {
                Assert.Equal("entity", ex.ParamName);
            }
        }

        [Fact]
        public void UpdatePost_PostDTO_not_null_but_not_belongs_to_user_return_0()
        {
            PostDTO postDTO = new PostDTO();
            List<Post> posts = new List<Post>();

            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            int error = postService.UpdatePost(postDTO, "khon");
            mockPostRepository.Verify(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()));
            mockPostRepository.VerifyNoOtherCalls();

            Assert.Equal(0, error);
        }

        [Fact]
        public void UpdatePost_PostDTO_not_null_and_belongs_to_user_should_be_updated()
        {
            PostDTO postDTO = new PostDTO();
            postDTO.Tags = new List<TagDTO>();
            Post post = new Post();
            post.Tags = new List<Tag>();
            List<Post> posts = new List<Post>();
            posts.Add(post);
            mockPostRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<Post, bool>>>()))
                .Returns((Expression<Func<Post, bool>> Predicate) => posts.AsQueryable());

            int error = postService.UpdatePost(postDTO, "khon");
            mockPostRepository.Verify(x => x.Update(It.IsAny<Post>()));
        }
    }
}
