﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogService.Service;
using BlogService.ServiceModule.AutoMapperProfile;
using DAL;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace BlogUnitTest.CategoryServiceTest
{
    
    public class CategoryServiceTest
    {
        Mock<IGenericRepository<Category>> mockCategoryRepository;
        Mock<IUnitOfWork> mockUnitOfWork;
        CategoryService categoryService;
        public CategoryServiceTest()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CategoryProfile>();
                cfg.AddProfile<PostProfile>();
            });
            mockCategoryRepository = new Mock<IGenericRepository<Category>>();
            mockUnitOfWork = new Mock<IUnitOfWork>();
            categoryService = new CategoryService(mockUnitOfWork.Object, mockCategoryRepository.Object);
        }
        [Fact]
        public void GetCategories_GetAll_should_be_call()
        {
            var categories = categoryService.GetCategories();
            mockCategoryRepository.Verify(x => x.GetAll());

            Assert.IsType<List<CategoryDTO>>(categories);
        }

        [Fact]
        public void GetPostsByCategory_should_return_categorydto_with_list_posts_within_pagesize_range()
        {
            Post post = new Post();
            Category category = new Category();
            category.Posts = new List<Post>();
            category.Posts.Add(post);
            category.Posts.Add(post);
            int pagesize = 5;
            mockCategoryRepository.Setup(x => x.FindById(It.IsAny<int>())).Returns(category);
            
            var categoryDTO = categoryService.GetPostsByCategory(1, 0, pagesize);

            Assert.InRange(categoryDTO.Posts.Count, 0, pagesize);
        }

        [Fact]
        public void GetTotalCategoryPost_should_return_int_total_count()
        {
            Post post = new Post();
            Category category = new Category();
            category.Posts = new List<Post>();
            category.Posts.Add(post);
            category.Posts.Add(post);
            category.Posts.Add(post);

            int expectedCount = 3;
            mockCategoryRepository.Setup(x => x.FindById(It.IsAny<int>())).Returns(category);

            var total = categoryService.GetTotalCategoryPost(1);

            Assert.Equal(expectedCount, total);
        }

        [Fact]
        public void GetPostsByCategories_GetAll_should_be_call()
        {
            Post post = new Post();
            Category category = new Category();
            category.Posts = new List<Post>();
            category.Posts.Add(post);
            category.Posts.Add(post);
            category.Posts.Add(post);
           
            List<Category> categories = new List<Category>();
            categories.Add(category);
            categories.Add(category);
            int expectedCount = 3;
            mockCategoryRepository.Setup(x => x.GetAll()).Returns(categories.AsQueryable());

            var categoryDTOs = categoryService.GetPostsByCategories();
            mockCategoryRepository.Verify(x => x.GetAll());

            Assert.Equal(expectedCount, categoryDTOs.First().Posts.Count);
        }
    }
}
