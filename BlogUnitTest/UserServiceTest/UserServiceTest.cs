﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogService.Service;
using BlogService.ServiceModule.AutoMapperProfile;
using DAL;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace BlogUnitTest.UserServiceTest
{
    public class UserServiceTest
    {
        Mock<IGenericRepository<User>> mockUserRepository;
        Mock<IUnitOfWork> mockUnitOfWork;
        UserService userService;
        public UserServiceTest()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<UserProfile>();
            });
            mockUserRepository = new Mock<IGenericRepository<User>>();
            mockUnitOfWork = new Mock<IUnitOfWork>();
            userService = new UserService(mockUnitOfWork.Object, mockUserRepository.Object);
        }
        [Fact]
        public void Login_when_login_user_should_exists()
        {
            userService.login("khon", "123456");
            mockUserRepository.Verify(y => y.FindAll(It.IsAny<Expression<Func<User, bool>>>()));
        }
        [Fact]
        public void GetUserId_should_return_id()
        {
            User user = new User();
            user.Id = 1;
            user.Username = "khon";

            List<User> users = new List<User>();
            users.Add(user);
            mockUserRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns((Expression<Func<User, bool>> Predicate) => users.AsQueryable());

            int userId = userService.GetUserId("khon");
            Assert.Equal(user.Id, userId);
        }
        [Fact]
        public void AddUser_if_userdto_null_should_throw_exception()
        {
            UserDTO userDTO = null;
            
            string expectedParameterName = "entity";
            try
            {
                userService.AddUser(userDTO);
            }
            catch (ArgumentNullException ex)
            {
                Assert.Equal(expectedParameterName, ex.ParamName);
            }
        }
        [Fact]
        public void AddUser_if_user_name_exists_return_negative()
        {
            UserDTO userDTO = new UserDTO();
            userDTO.Username = "khon";
            userDTO.Password = "123456";
            User user = new User();
            user.Username = "khon";
            user.Password = "123456";
            List<User> users = new List<User>();
            users.Add(user);

            mockUserRepository.Setup(x => x.FindAll(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns((Expression<Func<User, bool>> Predicate) => users.AsQueryable());

            int a = userService.AddUser(userDTO);

            Assert.Equal(-1, a);
        }
        [Fact]
        public void AddUser_if_userdto_not_null_and_not_exists_user_should_be_created()
        {
            UserDTO userDTO = new UserDTO();
            userDTO.Username = "khon";
            userDTO.Password = "123456";

            userService.AddUser(userDTO);
            
            mockUserRepository.Verify(y => y.Add(It.IsAny<User>()));
        }
        [Fact]
        public void DeleteUser_if_user_exists_user_should_be_deleted()
        {
            userService.DeleteUser("khon");
            mockUserRepository.Verify(y => y.Delete(It.IsAny<User>()));
        }
    }
}
