﻿using BlogDTO;
using BlogEntity.UtilityHelper;
using BlogService.IService;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Security;

namespace BlogWebAPI.Controllers
{
    [Authorize]
    public class ManagePostsController : ApiController
    {
        IPostService _postService;
        ITagService _tagService;
        public ManagePostsController(IPostService postService, ITagService tagService)
        {
            _postService = postService;
            _tagService = tagService;
        }
       
        [HttpPost]
        public HttpResponseMessage Create(PostDTO postDTO)
        {
            _postService.AddPost(postDTO);
            return Request.CreateResponse(HttpStatusCode.Created);  
        }

        [HttpGet]
        public HttpResponseMessage GetPostByUserName(int? id, string Username)
        {
            var post = _postService.GetPost(Username, (int)id);
            if (post == null)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            return Request.CreateResponse(HttpStatusCode.OK, post);
        }

        [HttpPut]
        public HttpResponseMessage Edit(PostDTO postDTO)
        {
            var postToUpdate = _postService.GetPost(postDTO.Username, postDTO.Id);
            if (postToUpdate == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Unable to save changes.The post was deleted by another user.");
            }

            int exceptionConcurrency = _postService.UpdatePost(postDTO, postDTO.Username);
            if (exceptionConcurrency == -1)
            {
                return Request.CreateResponse(HttpStatusCode.Redirect, "The record you attempted to edit "
                + "was modified by another user after you got the original value. The "
                + "edit operation was canceled. Please refresh the page the new value to display!");
            }
            if (exceptionConcurrency == -2)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Unable to save changes. The post was deleted by another user.");
            }
            if(exceptionConcurrency == -3)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Error");
            return Request.CreateResponse(HttpStatusCode.OK);

        }

        [HttpDelete]
        public HttpResponseMessage DeleteConfirmed(int? id, string userName)
        {
            _postService.DeletePost((int)id, userName);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
