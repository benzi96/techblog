﻿using AutoMapper;
using BlogDTO;
using BlogEntity.UtilityHelper;
using BlogService.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace BlogWebAPI.Controllers
{
    public class PostsController : ApiController
    {
        IPostService _postService;
        ITagService _tagService;
        public PostsController(IPostService postService, ITagService tagService)
        {
            _postService = postService;
            _tagService = tagService;
        }
        [HttpGet]
        public HttpResponseMessage Details(int? id)
        {
            if (id == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var post = _postService.GetPost((int)id);

            return Request.CreateResponse(HttpStatusCode.OK, post);
        }
        [HttpGet]
        public HttpResponseMessage GetUserPosts(string UserName, int? page)
        {
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            var posts = _postService.GetPosts(UserName, pageIndex, pageSize);

            return Request.CreateResponse(HttpStatusCode.OK, posts);
        }
        [HttpGet]
        public HttpResponseMessage GetTotalUserPosts(string Username)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _postService.GetTotalUserPosts(Username));
        }
        [HttpGet]
        public HttpResponseMessage Search(string term)
        {
            var posts = _postService.GetPostsForAutoComplete(term);
            var result = posts.Select(p => new
            {
                id = p.Id,
                text = p.Title,
                title = p.Title,
                author = p.Username,
                imgsrc = ImageHelper.GetImageTagSrc(p.PostImage),
                url = "/Posts/" + p.Id + "/" + p.UrlSlug,
            });
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        [HttpGet]
        public HttpResponseMessage RecentPosts()
        {
            var recentPosts = _postService.GetRecentPosts();
            return Request.CreateResponse(HttpStatusCode.OK, recentPosts);
        }
        [HttpGet]
        public HttpResponseMessage SearchPostResult(string term, string option)
        {
            if (option == "Title" || option == "Author")
            {
                List<PostDTO> posts = _postService.GetPosts(term, option);
                return Request.CreateResponse(HttpStatusCode.OK, posts);
            }
            else
            {
                List<TagDTO> tags = _tagService.GetTags(term);
                return Request.CreateResponse(HttpStatusCode.OK, tags);
            }
        }
    }
}
