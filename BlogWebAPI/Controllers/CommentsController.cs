﻿using BlogDTO;
using BlogService.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlogWebAPI.Controllers
{
    public class CommentsController : ApiController
    {
        IPostService _postService;
        ICommentService _commentService;
        public CommentsController(IPostService postService, ICommentService commentService)
        {
            _postService = postService;
            _commentService = commentService;
        }
        [HttpPost]
        public HttpResponseMessage Create(CommentDTO commentDTO)
        {
            commentDTO.Id = _postService.AddComment(commentDTO);
            return Request.CreateResponse(HttpStatusCode.OK, commentDTO);
        }
        [HttpDelete]
        public void Delete(int? Id, string userName)
        {
            if (Id != null) _commentService.DeleteComment((int)Id, userName);
        }
    }
}