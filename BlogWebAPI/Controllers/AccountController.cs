﻿using BlogDTO;
using BlogService.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace BlogWebAPI.Controllers
{
    [Authorize]
    public class AccountController : ApiController
    {
        IUserService _userService;
        ICategoryService _categoryService;
        public AccountController(IUserService userService, ICategoryService categoryService)
        {
            _userService = userService;
            _categoryService = categoryService;
           
        }

        [HttpPost, AllowAnonymous, Route("api/account/login")]
        public HttpResponseMessage Login(UserDTO userDTO)
        {
            if (_userService.login(userDTO.Username, userDTO.Password))
            {
                //FormsAuthentication.SetAuthCookie(userDTO.Username, false);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            //AuthenticationManager
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpPost, AllowAnonymous, Route("api/account/logoff")]
        public HttpResponseMessage LogOff()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost, AllowAnonymous, Route("api/account/signup")]
        public HttpResponseMessage Signup(UserDTO userDTO)
        {
            if (_userService.AddUser(userDTO) > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Username already exists");
            }
        }
    }
}
