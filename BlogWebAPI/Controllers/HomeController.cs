﻿using BlogService.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlogWebAPI.Controllers
{
    public class HomeController : ApiController
    {
        ICategoryService _categoryService;
        public HomeController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public HttpResponseMessage Index()
        {
            var categories = _categoryService.GetPostsByCategories();
            return Request.CreateResponse(HttpStatusCode.OK, categories);
        }
    }
}
