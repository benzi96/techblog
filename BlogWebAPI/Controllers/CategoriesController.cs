﻿using BlogService.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlogWebAPI.Controllers
{
    public class CategoriesController : ApiController
    {
        ICategoryService _categoryService;
        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        // GET api/<controller>
        [HttpGet]
        public HttpResponseMessage GetCategories()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _categoryService.GetCategories());
        }
        [HttpGet]
        public HttpResponseMessage Details(int? id, int? page)
        {
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            var category = _categoryService.GetPostsByCategory((int)id, pageIndex, pageSize);
            return Request.CreateResponse(HttpStatusCode.OK, category);
        }
        [HttpGet]
        public HttpResponseMessage GetTotalCategoryPost(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _categoryService.GetTotalCategoryPost(id));
        }
    }
}