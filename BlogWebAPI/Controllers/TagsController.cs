﻿using BlogService.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlogWebAPI.Controllers
{
    public class TagsController : ApiController
    {
        ITagService _tagService;
        public TagsController(ITagService tagService)
        {
            _tagService = tagService;
        }
        [HttpGet]
        public HttpResponseMessage Details(int? id, int? page)
        {
            var pageIndex = (page ?? 1) - 1;
            var pageSize = 15;
            var tagDTO = _tagService.GetPostsByTag((int)id, pageIndex, pageSize);
          
            return Request.CreateResponse(HttpStatusCode.OK, tagDTO);
        }
        [HttpGet]
        public HttpResponseMessage GetTotalTagPosts(int id)
        {
            return Request.CreateResponse(_tagService.GetTotalTagPosts(id));
        }
        [HttpGet]
        public HttpResponseMessage GetPopularTags()
        {
            var tags = _tagService.GetPopularTags();
            return Request.CreateResponse(HttpStatusCode.OK, tags);
        }
    }
}
