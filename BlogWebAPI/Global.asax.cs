﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using BlogService.ServiceModule;
using BlogService.ServiceModule.AutoMapperProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace BlogWebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule(new ServiceAutofacModule());

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TagProfile>();
                cfg.AddProfile<CommentProfile>();
                cfg.AddProfile<PostProfile>();
                cfg.AddProfile<CategoryProfile>();
            });
        }
    }
}
