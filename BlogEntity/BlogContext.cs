﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogEntity.Entity;
using BlogEntity.EntityMap;

namespace BlogEntity
{
    public class BlogContext : DbContext
    {
        static BlogContext()
        {
            Database.SetInitializer(new EntitiesContextInitializer());
        }

        public DbSet<Tag> Tags { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new PostMap());
            modelBuilder.Configurations.Add(new UserMap());
        }
    }

}
