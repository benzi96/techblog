﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEntity.Entity
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        //public virtual ICollection<Post> Posts { get; set; }
        //public virtual ICollection<Comment> Comments { get; set; }
    }

}
