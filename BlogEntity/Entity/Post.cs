﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEntity.Entity
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UrlSlug { get; set; }
        public byte[] PostImage { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public DateTime PostedOn { get; set; }
        public DateTime? Modified { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public byte[] RowVersion { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public Post()
        {
            this.Comments = new List<Comment>();
        }
    }
}
