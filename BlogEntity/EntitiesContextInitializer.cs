﻿using BlogEntity.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEntity
{
    public class EntitiesContextInitializer : DropCreateDatabaseIfModelChanges<BlogContext>
    {
        protected override void Seed(BlogContext context)
        {
            List<Category> categories = new List<Category>
            {
                new Category { Name="Software", UrlSlug="software"},
                new Category { Name="Hardware", UrlSlug="hardware"},
                new Category { Name="Education", UrlSlug="education"},
                new Category { Name="News", UrlSlug="news"},
                new Category { Name="Programming", UrlSlug="programming"}
            };

            // add data into context and save to db
            foreach (Category c in categories)
            {
                context.Category.Add(c);
            }
            context.SaveChanges();

        }
    }
}
