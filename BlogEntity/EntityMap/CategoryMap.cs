﻿using BlogEntity.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEntity.EntityMap
{
    public class CategoryMap : EntityTypeConfiguration<Category>
    {
        public CategoryMap()
        {
            HasMany(e => e.Posts)
            .WithOptional(e => e.Category)
            .HasForeignKey(e => e.CategoryId)
            .WillCascadeOnDelete(false);
        }
    }
}
