﻿using BlogEntity.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEntity.EntityMap
{
    public class PostMap : EntityTypeConfiguration<Post>
    {
        public PostMap()
        {
            Property(p => p.RowVersion)
                .IsRowVersion();

            HasMany(e => e.Tags)
            .WithMany(e => e.Posts)
            .Map(m => m.ToTable("TagPosts"));

           HasMany(e => e.Comments)
                .WithRequired()
                .WillCascadeOnDelete(true);
        }
    }
}
