﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDTO
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UrlSlug { get; set; }
        public byte[] PostImage { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public DateTime PostedOn { get; set; }
        public DateTime? Modified { get; set; }

        public int CategoryId { get; set; }
        public string Username { get; set; }

        public byte[] RowVersion { get; set; }

        public List<TagDTO> Tags { get; set; }
        public List<CommentDTO> Comments { get; set; }
    }
}
