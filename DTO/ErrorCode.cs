﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDTO
{
    public enum ErrorCode
    {
        ConcurrencyException,
        NullException,
        Exception
    }
}
