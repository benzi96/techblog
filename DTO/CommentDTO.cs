﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDTO
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public DateTime CommentedOn { get; set; }

        public string Username { get; set; }
        public int PostId { get; set; }
    }
}
