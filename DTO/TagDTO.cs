﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDTO
{
    public class TagDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlSlug { get; set; }
        public int NumberOfPosts { get; set; }
        public List<PostDTO> Posts { get; set; }
    }
}
