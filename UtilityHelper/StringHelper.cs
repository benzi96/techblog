﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BlogEntity.UtilityHelper
{
    public static class StringHelper
    {
        public static string GenerateSlug(string name)
        {
            string slug = Regex.Replace(Regex.Replace(Regex.Replace(name, @"\s+", "_"), @"\W", ""), "_+", "-");
            return slug;
        }

        public static string GetShortDescriptionfromBody(string body)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(body);
            string result = HtmlEntity.DeEntitize(htmlDoc.DocumentNode.InnerText);
            IEnumerable<string> words = result.Split().Take(50);
            StringBuilder firstwords = new StringBuilder();
            foreach (string s in words)
            {
                firstwords.Append(s + " ");
            }
            firstwords.Append("...");
            if (string.IsNullOrEmpty(result)) return "There is no content in this post";
            if (result.Length < 150) return firstwords.ToString();
            return firstwords.ToString();
        }

        public static string Encrypt(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static String CreateKey(int numBytes)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[numBytes];
            rng.GetBytes(buff);
            return BytesToHexString(buff);
        }

        private static String BytesToHexString(byte[] bytes)
        {
            StringBuilder hexString = new StringBuilder(64);
            for (int counter = 0; counter < bytes.Length; counter++)
            {
                hexString.Append(String.Format("{0:X2}", bytes[counter]));
            }
            return hexString.ToString();
        }
    }
}
