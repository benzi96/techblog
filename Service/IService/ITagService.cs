﻿using BlogDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.IService
{
    public interface ITagService
    {
        TagDTO GetPostsByTag(int Id, int pageIndex, int pageSize);
        int GetTotalTagPosts(int Id);
        List<TagDTO> GetPopularTags();
        List<TagDTO> GetTags(string term);
    }
}
