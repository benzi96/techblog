﻿using BlogDTO;
using BlogEntity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.IService
{
    public interface IPostService
    {
        int AddPost(PostDTO postDTO);
        int AddComment(CommentDTO commentDTO);
        int DeletePost(int postId, string Username);
        int UpdatePost(PostDTO postDTO, string userName);
        List<PostDTO> GetPostsForAutoComplete(string title);
        List<PostDTO> GetPosts(string Username, int pageIndex, int pageSize);
        int GetTotalUserPosts(string Username);
        List<PostDTO> GetPosts(string term, string option);
        List<PostDTO> GetRecentPosts();
        PostDTO GetPost(int id);
        PostDTO GetPost(string Username, int Id);
    }
}
