﻿using BlogDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.IService
{
    public interface ICategoryService
    {
        List<CategoryDTO> GetCategories();
        CategoryDTO GetPostsByCategory(int Id, int pageIndex, int pageSize);
        int GetTotalCategoryPost(int Id);
        List<CategoryDTO> GetPostsByCategories();
    }
}
