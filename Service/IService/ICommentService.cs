﻿using BlogDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.IService
{
    public interface ICommentService
    {
        int DeleteComment(int Id, string username);
    }
}
