﻿using BlogDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.IService
{
    public interface IUserService
    {
        int AddUser(UserDTO userDTO);
        int DeleteUser(string username);
        int UpdateUser(UserDTO userDTO);
        bool login(string username, string password);
        int GetUserId(string username);
    }
}
