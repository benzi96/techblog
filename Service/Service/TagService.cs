﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogService.IService;
using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.Service
{
    public class TagService : ITagService
    {
        IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Tag> _tagRepository;

        public TagService(IUnitOfWork unitOfWork, IGenericRepository<Tag> tagRepository)
        {
            _unitOfWork = unitOfWork;
            _tagRepository = tagRepository;
        }

        public TagDTO GetPostsByTag(int Id, int pageIndex, int pageSize)
        {
            var tag = _tagRepository.FindById(Id);
            var posts = tag.Posts.OrderByDescending(p => p.PostedOn).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            TagDTO tagDTO = new TagDTO();
            Mapper.Map(tag, tagDTO);
            tagDTO.Posts = Mapper.Map<List<PostDTO>>(posts);
            return tagDTO;
        }
        public int GetTotalTagPosts(int Id)
        {
            return _tagRepository.FindById(Id).Posts.Count();
        }
        public List<TagDTO> GetPopularTags()
        {
            List<TagDTO> tagDTOs = new List<TagDTO>();
            var tags = _tagRepository.GetAll().OrderByDescending(t => t.Posts.Count()).Take(8).ToList();
            Mapper.Map(tags, tagDTOs);
            return tagDTOs;
        }
        //public List<TagDTO> GetTags(string term)
        //{
        //    List<TagDTO> tagDTOs = new List<TagDTO>();
        //    var tags = _tagRepository.FindAll(t => t.Name.Contains(term)).Take(20).ToList();
        //    Mapper.Map(tags, tagDTOs);
        //    return tagDTOs;
        //}
        public List<TagDTO> GetTags(string term)
        {
            List<TagDTO> tagDTOs = new List<TagDTO>();
            var tags = _tagRepository.FindAll(t => t.Name.Contains(term)).Take(20).ToList();
            tagDTOs = Mapper.Map<List<TagDTO>>(tags);
            return tagDTOs;
        }


    }
}
