﻿using AutoMapper.QueryableExtensions;
using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogEntity.UtilityHelper;
using BlogService.IService;
using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.Service
{
    public class PostService : IPostService
    {
        IUnitOfWork _unitOfWork;
        IUserService _userService;
        private readonly IGenericRepository<Post> _postRepository;
        private readonly IGenericRepository<Tag> _tagRepository;

        public PostService(IUnitOfWork unitOfWork, IGenericRepository<Post> postRepository, IGenericRepository<Tag> tagRepository, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _postRepository = postRepository;
            _tagRepository = tagRepository;
            _userService = userService;
        }
        
        public PostDTO GetPost(int Id)
        {
            var post = _postRepository.FindAll(p => p.Id == Id)
                                      .Include(p => p.Comments)
                                      .Include(p => p.Tags)
                                      .FirstOrDefault();
            PostDTO postDTO = Mapper.Map<PostDTO>(post);
            postDTO.Comments = Mapper.Map<List<CommentDTO>>(post.Comments.OrderByDescending(c => c.CommentedOn));
            postDTO.Tags = Mapper.Map<List<TagDTO>>(post.Tags);
            return postDTO;
        }
        public PostDTO GetPost(string Username, int Id)
        {
            var post = _postRepository.FindAll(p => p.Id == Id && p.User.Username == Username)
                                      .Include(p => p.Tags)
                                      .FirstOrDefault();
            if (post == null) return null;
            PostDTO postDTO = new PostDTO();
            Mapper.Map(post, postDTO);
            postDTO.Tags = Mapper.Map<List<TagDTO>>(post.Tags);
            
            return postDTO;
        }

        public List<PostDTO> GetPosts(string Username, int pageIndex, int pageSize)
        {
            var posts = _postRepository.FindAll(post => post.User.Username == Username)
                                       .OrderByDescending(p => p.PostedOn)
                                       .Skip(pageIndex*pageSize)
                                       .Take(pageSize).ToList();
            return Mapper.Map<List<PostDTO>>(posts);
        }

        public int GetTotalUserPosts(string Username)
        {
            return _postRepository.FindAll(post => post.User.Username == Username).Count();
        }
        public List<PostDTO> GetPosts(string term, string option)
        {
            List<Post> posts = new List<Post>();
            if(option == "Author")
                posts = _postRepository.FindAll(post => post.User.Username.Contains(term)).Take(15).ToList();
            else
                posts= _postRepository.FindAll(post => post.Title.Contains(term)).Take(15).ToList();
            return Mapper.Map<List<PostDTO>>(posts);
        }
        public List<PostDTO> GetPostsForAutoComplete(string title)
        {
            var posts = _postRepository.FindAll(post => post.Title.Contains(title)).Take(8).ToList();
            return Mapper.Map<List<PostDTO>>(posts);
        }

        public List<PostDTO> GetRecentPosts()
        {
            DateTime recent = DateTime.Now.AddDays(-1);
            var posts = _postRepository.FindAll(p => p.PostedOn > recent)
                                       .OrderByDescending(p => p.PostedOn)
                                       .Take(6).ToList();
            return Mapper.Map<List<PostDTO>>(posts);
        }

        public void AddTagstoPost(Post post, List<TagDTO> tagDTOs)
        {
            post.Tags = new List<Tag>();
            foreach (TagDTO tagDTO in tagDTOs)
            {
                Tag tag = _tagRepository.FindAll(t => t.Name == tagDTO.Name).FirstOrDefault();
                if (tag == null)
                {
                    tag = Mapper.Map<Tag>(tagDTO);
                     _tagRepository.Add(tag);
                }
                post.Tags.Add(tag);
            }
        }
        public void RemoveTagsFromPost(Post post)
        {
            List<int> tagids = new List<int>();
            foreach (Tag tag in post.Tags)
            {
                tagids.Add(tag.Id);
            }
            foreach (int id in tagids)
            {
                Tag tag = _tagRepository.FindById(id);
                post.Tags.Remove(tag);
            }
        }
        public int AddComment(CommentDTO commentDTO)
        {
            Comment comment = Mapper.Map<Comment>(commentDTO);
            comment.UserId = _userService.GetUserId(commentDTO.Username);
            Post post = _postRepository.FindById(commentDTO.PostId);
            if (post.Comments == null)
                post.Comments = new List<Comment>();
            post.Comments.Add(comment);
            _postRepository.Update(post);
            _unitOfWork.Commit();
            return comment.Id;
        }
        public int AddPost(PostDTO entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            
            Post post = Mapper.Map<Post>(entity);
            post.UserId = _userService.GetUserId(entity.Username);
            post.PostedOn = DateTime.Now;
            post.PostImage = entity.PostImage;
            AddTagstoPost(post, entity.Tags);
            _postRepository.Add(post);
            return _unitOfWork.Commit();
        }
        public int DeletePost(int Id, string Username)
        {
            var post = _postRepository.FindAll(p => p.Id == Id && p.User.Username == Username).FirstOrDefault();
            if (post == null) return 0;

            _postRepository.Delete(post);
            return _unitOfWork.Commit();
        }
        public int UpdatePost(PostDTO entity, string userName)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            var post = _postRepository.FindAll(p => p.Id == entity.Id && p.User.Username== userName).Include(p => p.Tags).ToList().FirstOrDefault();
            if (post == null) return 0;

            RemoveTagsFromPost(post);
            Mapper.Map(entity, post);
            if (entity.PostImage != null) post.PostImage = entity.PostImage;
            post.Modified = DateTime.Now;
            AddTagstoPost(post, entity.Tags);
            _postRepository.SetOriginalValues(post, entity.RowVersion);
            _postRepository.Update(post);
            return _unitOfWork.Commit();
            
        }
    }
}
