﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogService.IService;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.Service
{
    public class CommentService : ICommentService
    {
        IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Comment> _commentRepository;

        public CommentService(IUnitOfWork unitOfWork, IGenericRepository<Comment> commentRepository)
        {
            _unitOfWork = unitOfWork;
            _commentRepository = commentRepository;
        }

        public int DeleteComment(int Id, string username)
        {
            var comment = _commentRepository.FindAll(c => c.Id == Id && c.User.Username == username).FirstOrDefault();
            if (comment == null) return -1;
            _commentRepository.Delete(comment);
            return _unitOfWork.Commit();
        }
    }
}
