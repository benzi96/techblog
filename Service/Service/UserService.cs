﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogEntity.UtilityHelper;
using BlogService.IService;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.Service
{
    public class UserService : IUserService
    {
        IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<User> _userRepository;

        public UserService(IUnitOfWork unitOfWork, IGenericRepository<User> userRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
        }

        public bool login(string username, string password)
        {
            string encryptedPassword = StringHelper.Encrypt(password);
            return _userRepository.FindAll(user => user.Username == username && user.Password == encryptedPassword).Any();
        }
        private bool IsUsernameExist(string username)
        {
            return _userRepository.FindAll(user => user.Username == username).Any();
        }
        public int GetUserId(string username)
        {
            return _userRepository.FindAll(user => user.Username == username).FirstOrDefault().Id;
        }
        public int AddUser(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException("entity");
            }
            if (IsUsernameExist(userDTO.Username)) return -1;

            User user = Mapper.Map<User>(userDTO);
            _userRepository.Add(user);

            return _unitOfWork.Commit();
        }
        public int DeleteUser(string username)
        {
            User user = _userRepository.GetAll().FirstOrDefault(u => u.Username == username);
            _userRepository.Delete(user);
            return _unitOfWork.Commit();
        }
        public int UpdateUser(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException("entity");
            }
            User user = Mapper.Map<User>(userDTO);
            _userRepository.Update(user);
            return _unitOfWork.Commit();
        }
    }
}
