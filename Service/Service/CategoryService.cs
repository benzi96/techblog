﻿using BlogDTO;
using BlogEntity.Entity;
using BlogService.IService;
using DAL;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;

namespace BlogService.Service
{
    public class CategoryService : ICategoryService
    {
        IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Category> _categoryRepository;

        public CategoryService(IUnitOfWork unitOfWork, IGenericRepository<Category> categoryRepository)
        {
            _unitOfWork = unitOfWork;
            _categoryRepository = categoryRepository;
        }

        public List<CategoryDTO> GetCategories()
        {
            var categories = _categoryRepository.GetAll().ToList();
            List<CategoryDTO> categoryDTOs = new List<CategoryDTO>();
            Mapper.Map(categories, categoryDTOs);
            return categoryDTOs;
        }
        public CategoryDTO GetPostsByCategory(int Id, int pageIndex, int pageSize)
        {
            var category = _categoryRepository.FindById(Id);
            var posts = category.Posts.OrderByDescending(p => p.PostedOn).Skip(pageIndex * pageSize).Take(pageSize);
            CategoryDTO categoryDTO = new CategoryDTO();
            Mapper.Map(category, categoryDTO);
            categoryDTO.Posts = Mapper.Map<List<PostDTO>>(posts);
            return categoryDTO;
        }
        public int GetTotalCategoryPost(int Id)
        {
            return _categoryRepository.FindById(Id).Posts.Count();
        }

        public List<CategoryDTO> GetPostsByCategories()
        {
            var categories = _categoryRepository.GetAll()
                                                .Select(c => new
                                                {
                                                    Id = c.Id,
                                                    Name = c.Name,
                                                    UrlSlug = c.UrlSlug,
                                                    Posts = c.Posts.OrderByDescending(p => p.PostedOn).Take(3)
                                                })
                                                .AsEnumerable()
                                                .Select(c => c).ToList();
            List<CategoryDTO> categoryDTOs = Mapper.Map<List<CategoryDTO>>(categories);
            return categoryDTOs;
        }
    }
}
