﻿using Autofac;
using BlogService.IService;
using BlogService.Service;
using DAL.DALModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule
{
    public class ServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new DALAutofacModule());
            builder.RegisterType(typeof(UserService)).As(typeof(IUserService)).InstancePerRequest();
            builder.RegisterType(typeof(PostService)).As(typeof(IPostService)).InstancePerRequest();
            builder.RegisterType(typeof(CategoryService)).As(typeof(ICategoryService)).InstancePerRequest();
            builder.RegisterType(typeof(CommentService)).As(typeof(ICommentService)).InstancePerRequest();
            builder.RegisterType(typeof(TagService)).As(typeof(ITagService)).InstancePerRequest();
        }
    }
}
