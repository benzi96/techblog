﻿using AutoMapper;
using BlogService.ServiceModule.AutoMapperProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule
{
    public class AutoMapperConfiguration
    {
        public MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<CommentProfile>();
                cfg.AddProfile<PostProfile>();
                cfg.AddProfile<CategoryProfile>();
            });
            return config;
        }
    }
}
