﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogEntity.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule.AutoMapperProfile
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDTO, User>()
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => StringHelper.Encrypt(src.Password)))
                .ForMember(dest => dest.Id, opt => opt.Ignore());
        }
    }
}
