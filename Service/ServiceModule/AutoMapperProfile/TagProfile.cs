﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogEntity.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule.AutoMapperProfile
{
    public class TagProfile : Profile
    {
        public TagProfile()
        {
            CreateMap<TagDTO, Tag>()
                .ForMember(dest => dest.UrlSlug, opt => opt.MapFrom(src => StringHelper.GenerateSlug(src.Name)));
            CreateMap<Tag, TagDTO>()
                .ForMember(dest => dest.NumberOfPosts, opt => opt.MapFrom(src => src.Posts.Count()))
                .ForMember(dest => dest.Posts, opt => opt.Ignore());
        }
    }
}
