﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule.AutoMapperProfile
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<CategoryDTO, Category>();
            CreateMap<Category, CategoryDTO>()
                .ForMember(dest => dest.Posts, opt => opt.Ignore());
        }
    }
}
