﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule.AutoMapperProfile
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMap<CommentDTO, Comment>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore());

            CreateMap<Comment, CommentDTO>().
                ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.Username));
        }
    }
}
