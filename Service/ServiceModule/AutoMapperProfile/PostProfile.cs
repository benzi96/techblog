﻿using AutoMapper;
using BlogDTO;
using BlogEntity.Entity;
using BlogEntity.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogService.ServiceModule.AutoMapperProfile
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<PostDTO, Post>()
                .ForMember(dest => dest.UrlSlug, opt => opt.MapFrom(src => StringHelper.GenerateSlug(src.Title)))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => StringHelper.GetShortDescriptionfromBody(src.Body)))
                .ForMember(dest => dest.PostedOn, opt => opt.Ignore())
                .ForMember(dest => dest.Comments, opt => opt.Ignore())
                .ForMember(dest => dest.Tags, opt => opt.Ignore())
                .ForMember(dest => dest.PostImage, opt => opt.Ignore())
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            

            CreateMap<Post, PostDTO>()
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.Username))
                .ForMember(dest => dest.Comments, opt => opt.Ignore())
                .ForMember(dest => dest.Tags, opt => opt.Ignore());
        }
    }
}
